#include<stdio.h>
#include<stdlib.h>
#include<time.h>


void cabeca();
void peca(void);
void escolhe(void);
void mostrar1(void);
void mostrar2(void);
void comecar(void);
void jogada(void);
void jogador1(void);
void jogador2(void);


int pecas_jogador[12][2];
int pecas[28][3]={{0}};
int cabecas[2];
int num_jogadas[2]={0};
int venceu;
char c;
int contagem[2]={0};

int main(void)
{
	srand(time(NULL));
	cabeca();
	printf("      DOMINO\n");
	cabeca();
	printf("Escolha o primeiro e o segundo jogador...\n");
	getchar();
	peca();
	escolhe();
	comecar();

	return 0;
}

void cabeca()    
{
	int i;
		
	for(i=1;i<20;i++)
		putchar('*');
	putchar('\n');
}



void peca()                                           /* aloca as pecas como vetores */
{
	int j,i,l=0;

	for(j=0; j<7; j++)
	{
		for(i=j; i<7; i++)
		{
			pecas[l][0]=j;
			pecas[l][1]=i;
			l++;
		}
	}	
}

void escolhe()                                               /* sorteia as pecas para cada jogador */
{
	int jogador,i,l;
	for(jogador=0;jogador<2;jogador++)
	{
		for(i=0; i<12;i++)
		{
			do
			{
				l=rand()%28;
			}while(pecas[l][2]!=0);
			pecas[l][2]=1;
			pecas_jogador[i][jogador]=l;
		}
	}
}
void mostrar1()                                             /* mostra para jogador suas pecas */
{
        int l,i;
		contagem[0]=0;
        getchar();
        printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nO jogador 1 vai jogar.\n");
        getchar();
        printf("As pecas do jogador 1 sao:\n");
        num_jogadas[0]=0;
        for(i=0;i<12;i++)
        {
                l = pecas_jogador[i][0];
                if (pecas[l][0]!=9)                                                  /* condicao para nao mostrar as pecas ja jogadas */
                {
                        printf("%d : %d = %d\n",pecas[l][0],pecas[l][1],l);
                        num_jogadas[0]++;                                               /* condicao para para saber quantas pecas restam para o jogador */
   						contagem[0]=contagem[0]+pecas[l][0]+pecas[l][1];
				 }
        }
        printf("Para o toque digite o numero de qualquer peca que ainda nao foi jogada\nPara contagem dos pontos aperte 'c'\n");
        c=getchar();
}


void mostrar2()
{
        int l,i;
		contagem[1]=0;
        getchar();
        printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nO jogador 2 vai jogar.\n");
        getchar();
 	    printf("As pecas do jogador 2 sao:\n");
        num_jogadas[1]=0;
        for(i=0;i<12;i++)
        {
                l = pecas_jogador[i][1];
                if (pecas[l][0]!=9)
                {
                        printf("%d : %d = %d\n",pecas[l][0],pecas[l][1],l);
                        num_jogadas[1]++;
						contagem[1]=contagem[1]+pecas[l][0]+pecas[l][1];
                }
        }
        printf("Para o toque digite o numero de qualquer peca que ainda nao foi jogada\nPara contagem dos pontos aperte 'c'\n");
        c=getchar();
}
void comecar()                                           /* primeira jogada da partida, escolhe a maior carroca */
{
        int i,l;
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][0];
                if(l==27)
                {
                        printf("O primeiro começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=6;
                        cabecas[1]=6;
                        pecas[l][0]=9;
                        jogador2();
                        return;
                }
        }

        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][1];
                if(l==27)
                {
                        printf("O segundo jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=6;
                        cabecas[1]=6;
                        pecas[l][0]=9;
                        jogador1();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][0];
                if(l==25)
                {
                        printf("O primeiro jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=5;
                        cabecas[1]=5;
                        pecas[l][0]=9;
                        jogador2();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][1];
                if(l==25)
                {
                        printf("O segundo jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=5;
                        cabecas[1]=5;
                        pecas[l][0]=9;
                        jogador1();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][0];
                if(l==22)
                {
                        printf("O primeiro jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=4;
                        cabecas[1]=4;
                        pecas[l][0]=9;
                        jogador2();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][1];
                if(l==22)
                {
                        printf("O segundo jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=4;
                        cabecas[1]=4;
                        pecas[l][0]=9;
                        jogador1();
                        return;
                }
        }

        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][0];
                if(l==18)
                {
                        printf("O primeiro jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=3;
                        cabecas[1]=3;
                        pecas[l][0]=9;
                        jogador2();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][1];
                if(l==18)
                {
                        printf("O segundo jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=3;
                        cabecas[1]=3;
                        pecas[l][0]=9;
                        jogador1();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][0];
                if(l==13)
                {
                        printf("O primeiro jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=2;
                        cabecas[1]=2;
                        pecas[l][0]=9;
                        jogador2();
                        return;
                }
        }
        for(i=0;i<12;i++)
        {
                l=pecas_jogador[i][1];
                if(l==13)
                {
                        printf("O segundo jogador começou com %d : %d\n", pecas[l][0],pecas[l][1]);
                        cabecas[0]=2;
                        cabecas[1]=2;
                        pecas[l][0]=9;
                        jogador1();
                        return;
                }
        }
}
void jogada()                                               /* o jogador decide qual peca jogara */
{
        int i,n;
        printf("MESA |%d : %d|\n",cabecas[0],cabecas[1]);
        if (num_jogadas[0]==1)                                  /* se o jogador so tem uma peca a condicao eh verdadeira e ele pode bater ou nao */
        {
                printf("Vai bater ou toca?\n");
                scanf("%d",&i);
                if (pecas[i][0]!=cabecas[0] && pecas[i][0]!=cabecas[1] && pecas[i][1]!=cabecas[1] && pecas[i][1]!=cabecas[0])
                {
                        num_jogadas[0]=9;                                /* assumi um valor a variavel para a jogada de um jogador nao influenciar a do outro */
                        return;
                }
                else
                {
                        printf("Parabens, voce venceu!!!\n");
                        venceu=1;                                         /* variavel global que sera utilizada na funcao jogador para encerrar o jogo */
                        return;
                }
        }
        else
        {
                if (num_jogadas[1]==1)
                {
                        printf("Vai bater ou toca?\n");
                        scanf("%d",&i);
                        if (pecas[i][0]!=cabecas[0] && pecas[i][0]!=cabecas[1] && pecas[i][1]!=cabecas[1] && pecas[i][1]!=cabecas[0])
                        {
                                num_jogadas[1]=9;
                                return;
                        }
                        else
                        {
                                printf("Parabens, voce venceu!!!\n");
                                venceu=1;
                                return;
                        }
                }
        }

        printf("Escolha uma peca...\n");
        scanf("%d",&i);

        if((pecas[i][0]==cabecas[0]) && (pecas[i][1]==cabecas[1]))  /* se houver duas opcoes de jogada o jogador deve escolher */
        {
                printf("Onde voce quer jogar?\n");
                scanf("%d",&n);
                if(n==cabecas[0])
                {
                        cabecas[0]=pecas[i][1];
                        pecas[i][0]=9;
                        return;
                }
                else
                {
                        cabecas[1]=pecas[i][0];
                        pecas[i][0]=9;
                        return;
                }
        }
        if((pecas[i][0]==cabecas[1]) && (pecas[i][1]==cabecas[0]))
        {
                printf("Onde voce quer jogar?\n");
                scanf("%d",&n);
                if(n==cabecas[0])
                {
                        cabecas[0]=pecas[i][0];
                        pecas[i][0]=9;
                        return;
                }
                else
                {
                        cabecas[1]=pecas[i][1];
                        pecas[i][0]=9;
                        return;
                }
        }
                if(pecas[i][0] == cabecas[1])
                {
                        cabecas[1]= pecas[i][1];
                        pecas[i][0]=9;
                        return;
                }
                if(pecas[i][1] == cabecas[1])
                {
                        cabecas[1]= pecas[i][0];
                        pecas[i][0]=9;
                        return;
                }
                if(pecas[i][0] == cabecas[0])
                {
                        cabecas[0]= pecas[i][1];
                        pecas[i][0]=9;
                        return;
                }
                if(pecas[i][1] == cabecas[0])
                {
                        cabecas[0]= pecas[i][0];
                        pecas[i][0]=9;
                        return;
                }
}

void jogador2()
{
        mostrar2();
        if(c=='c')
        {
                if(contagem[1]==contagem[0])
					printf("Empatou!\nOs dois jogadores fizeram %d pontos\n",contagem[0]);
                if(contagem[0]<contagem[1])
			        printf("O jogador 1 venceu!\nO jogdor 1 fez %d pontos e o jogador 2 fez %d pontos\n",contagem[0],contagem[1]);           
			    if(contagem[0]>contagem[1])
			        printf("O jogador 2 venceu!\nO jogador 1 fez %d pontos e o jogador 2 fez %d pontos\n", contagem[0],contagem[1]);
            	return;
        }

        jogada();
        if (venceu==1)
                return;
        jogador1();
}

void jogador1()
{
        mostrar1();
          if(c=='c')
        {
                if(contagem[1]==contagem[0])
			        printf("Empatou!\nOs dois jogadores fizeram %d pontos\n",contagem[0]);
			    if(contagem[0]<contagem[1])
            		printf("O jogador 1 venceu!\nO jogdor 1 fez %d pontos e o jogador 2 fez %d pontos\n",contagem[0],contagem[1]);	             
			    if(contagem[0]>contagem[1])
			        printf("O jogador 2 venceu!\nO jogador 1 fez %d pontos e o jogador 2 fez %d pontos\n", contagem[0],contagem[1]);
            	return;
        }

        jogada();
        if(venceu==1)
                return;
        jogador2();
}
